package com.sf.usrs.client;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(UsrsClient.class)
@ConditionalOnMissingBean(UsrsClient.class)
@EnableConfigurationProperties(UsrsClientProperties.class)
public class UsrsClientAutoConfiguration {

    @Bean
    public UsrsClient client(UsrsClientProperties properties) {
        return UsrsClient.create(properties.getUrl());
    }
}
