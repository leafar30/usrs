package com.sf.usrs.client;

import com.sf.usrs.dto.AuthRequestDto;
import com.sf.usrs.dto.UserResponseDto;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public final class UsrsClient {

    private final WebClient client;

    private UsrsClient(String url) {
        this.client = WebClient.builder()
                .baseUrl(url)
                .build();
    }

    public static UsrsClient create(String url) {
        return new UsrsClient(url);
    }

    public Mono<UserResponseDto> authenticate(AuthRequestDto request) {
        return client.post()
                .uri("/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(request))
                .exchange()
                .filter(resp -> resp.statusCode().is2xxSuccessful())
                .switchIfEmpty(Mono.defer(() -> Mono.error(new AuthFailedException())))
                .flatMap(resp -> resp.body(BodyExtractors.toMono(UserResponseDto.class)));
    }

}
