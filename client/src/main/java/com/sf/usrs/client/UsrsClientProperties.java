package com.sf.usrs.client;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "sf.usrs")
public class UsrsClientProperties {

    @Configuration
    @Profile("local")
    @PropertySource(value = "classpath:usrs-client.properties")
    static class Staging {

    }

    private String url;
}
