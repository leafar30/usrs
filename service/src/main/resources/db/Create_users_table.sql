CREATE TABLE users (
  id UUID PRIMARY KEY,
  email varchar(200),
  password varchar(50),
  firstName varchar(200),
  lastName varchar(200),7y
  phone varchar(30),
  available BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE UNIQUE INDEX udx_users_email ON users(email);
CREATE UNIQUE INDEX udx_users_phone ON users(phone);