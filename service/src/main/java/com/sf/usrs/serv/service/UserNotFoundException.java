package com.sf.usrs.serv.service;

import java.util.UUID;
import lombok.Data;


@Data
public class UserNotFoundException extends RuntimeException {

    private final UUID id;

}
