package com.sf.usrs.serv.converter;

import com.sf.usrs.dto.UserResponseDto;
import com.sf.usrs.serv.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserResponseDto implements Converter<User, UserResponseDto> {
    @Override
    public UserResponseDto convert(final User user) {
        return UserResponseDto.builder()
                   .id(user.getId())
                   .email(user.getEmail())
                   .firstName(user.getFirstName())
                   .lastName(user.getLastName())
                   .phone(user.getPhone())
                   .build();
    }
}
