package com.sf.usrs.serv.model;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByEmailAndAvailableIsTrue(String email);
    Optional<User> findByPhoneAndAvailableIsTrue(String email);
    Optional<User> findByIdAndAvailableIsTrue(UUID id);
}