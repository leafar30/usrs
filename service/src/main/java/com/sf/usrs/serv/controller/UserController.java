package com.sf.usrs.serv.controller;

import com.sf.usrs.dto.CreateUserRequestDto;
import com.sf.usrs.dto.UserResponseDto;
import com.sf.usrs.serv.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/user/create")
    public ResponseEntity<UserResponseDto> createUser(@Valid @RequestBody CreateUserRequestDto request){
        UserResponseDto userResponseDto = userService.create(request);
        return ResponseEntity.ok(userResponseDto);
    }



}
