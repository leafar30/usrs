package com.sf.usrs.serv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsrsApplication.class, args);
	}

}

