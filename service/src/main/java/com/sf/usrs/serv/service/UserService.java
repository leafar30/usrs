package com.sf.usrs.serv.service;

import com.sf.usrs.dto.AuthRequestDto;
import com.sf.usrs.dto.CreateUserRequestDto;
import com.sf.usrs.dto.UpdateUserRequestDto;
import com.sf.usrs.dto.UserResponseDto;
import com.sf.usrs.serv.converter.CreateUserRequestDtoToUser;
import com.sf.usrs.serv.converter.UserToUserResponseDto;
import com.sf.usrs.serv.model.User;
import com.sf.usrs.serv.model.UserRepository;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CreateUserRequestDtoToUser createUserRequestDtoToUser;

    @Autowired
    private UserToUserResponseDto userToUserResponseDto;

    @Transactional
    public UserResponseDto update(final UpdateUserRequestDto updateUserRequestDto) {
        User user = findById(updateUserRequestDto.getId());
        String email = updateUserRequestDto.getEmail();
        String phone = updateUserRequestDto.getPhone();
        String firstName = updateUserRequestDto.getFirstName();
        String lastName = updateUserRequestDto.getLastName();

        if (email != null && !email.isEmpty()) {
            user.setEmail(email);
        }
        if (phone != null && !phone.isEmpty()) {
            user.setPhone(phone);
        }
        if (firstName != null && !firstName.isEmpty()) {
            user.setFirstName(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            user.setLastName(lastName);
        }

        User updatedUser = userRepository.save(user);
        return userToUserResponseDto.convert(updatedUser);
    }

    @Transactional
    public void delete(UUID id) {
        User user = findById(id);
        user.setAvailable(false);
        userRepository.save(user);
    }

    @Transactional
    public void recover(UUID id) {
        User user = findById(id);
        user.setAvailable(true);
        userRepository.save(user);
    }

    @Transactional
    public UserResponseDto create(CreateUserRequestDto userDto) {

        User user = createUserRequestDtoToUser.convert(userDto);
        user.setAvailable(true);
        User newUser = userRepository.save(user);
        return userToUserResponseDto.convert(newUser);
    }

    @Transactional
    public User findById(UUID id) {
        return userRepository.findByIdAndAvailableIsTrue(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Transactional
    public Optional<UserResponseDto> authenticate(final AuthRequestDto authDto) {
        Optional<User> userOptional = userRepository.findByEmailAndAvailableIsTrue(authDto.getEmail());

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            boolean samePass = user.getPassword().equals(authDto.getPassword());
            return samePass? Optional.of(userToUserResponseDto.convert(user)):Optional.empty();
        } else {
            return Optional.empty();
        }
    }
}
