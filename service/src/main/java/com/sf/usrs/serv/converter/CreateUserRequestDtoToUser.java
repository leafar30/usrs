package com.sf.usrs.serv.converter;

import com.sf.usrs.dto.CreateUserRequestDto;
import com.sf.usrs.serv.model.User;
import java.util.UUID;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CreateUserRequestDtoToUser implements Converter<CreateUserRequestDto, User> {
    @Override
    public User convert(final CreateUserRequestDto userDto) {
        return User.builder()
                   .id(UUID.randomUUID())
                   .email(userDto.getEmail())
                   .firstName(userDto.getFirstName())
                   .lastName(userDto.getLastName())
                   .password(userDto.getPassword())
                   .phone(userDto.getPhone())
                   .build();
    }
}
