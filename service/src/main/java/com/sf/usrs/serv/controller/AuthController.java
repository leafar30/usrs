package com.sf.usrs.serv.controller;

import com.sf.usrs.dto.AuthRequestDto;
import com.sf.usrs.dto.UserResponseDto;
import com.sf.usrs.serv.service.UserService;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/auth")
    public ResponseEntity<UserResponseDto> auth(@Valid @RequestBody AuthRequestDto request){

        Optional<UserResponseDto> maybe = userService.authenticate(request);

        if(maybe.isEmpty()){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(maybe.get());
    }
}
