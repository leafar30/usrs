package com.sf.usrs.dto;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Builder
@Data
public class UserResponseDto {

    private final UUID id;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final String phone;

}
