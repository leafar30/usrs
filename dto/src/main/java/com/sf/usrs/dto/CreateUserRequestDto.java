package com.sf.usrs.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CreateUserRequestDto {
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
    private String password;
}
