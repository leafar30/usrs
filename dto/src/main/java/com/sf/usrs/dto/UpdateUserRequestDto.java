package com.sf.usrs.dto;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UpdateUserRequestDto {
    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
    private String password;
}
