package com.sf.usrs.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Builder
@Data
public class AuthRequestDto {

    private final String email;
    private final String password;
}
